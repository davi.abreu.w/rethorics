import tones
import modes
import chord
import guitar

def test():
    notes = tones.Notes()

    def test_find_scale_mode():
        notes.show_type()
        all_modes = [
            modes.MajorMode(notes),
            modes.MinorMode(notes),
            modes.DoricMode(notes),
            modes.FridgeoMode(notes),
            modes.LidiumMode(notes),
            modes.MixolidiumMode(notes),
            modes.LokriMode(notes)
        ]

        for note in notes.all_notes:
            print("Searching available modes for {} note".format(note))
            print("#################################")
            for mode in all_modes:
                scale = mode.find_scale(note)
                if scale:
                    print("Searching for `{}` for `{}` note".format(mode, note))
                    print(scale)
                    print()
            print("#################################")
            print()

    def test_print_chord():
        notes.hide_type()
        c = chord.Chord(notes, 1, notes.A)
        print(c)

    def test_e_minor_mode_guitar_print():
        notes.hide_type()
        g = guitar.Guitar(notes)
        minor = modes.MinorMode(notes)
        note = notes.E
        print("Searching scale for `{}` for `{}` note".format(minor, note))
        scale = minor.find_scale(notes.E)
        g.print_scale(scale)

    # test_find_scale_mode()
    # test_print_chord()
    test_e_minor_mode_guitar_print()

if __name__ == "__main__":
    test()
