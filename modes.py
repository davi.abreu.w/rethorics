from tones import ToneType


class ScaleMode():

    def __init__(self, notes, tone_type_sequence):
        self.notes = notes
        self.tone_type_sequence = tone_type_sequence
        self.name = "Scale Mode"

    def find_scale(self, base_note):
        if base_note.type != self.tone_type_sequence[0]:
            return None
        scale = [base_note]
        last_note = base_note
        for tone_type in self.tone_type_sequence[1:]:
            last_note = self.notes.find_next_note(last_note, tone_type)
            scale.append(last_note)
        return scale

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return self.name


class MajorMode(ScaleMode):

    def __init__(self, notes):
        tone_type_sequence = [
            ToneType.TONE,
            ToneType.TONE,
            ToneType.SEMITONE,
            ToneType.TONE,
            ToneType.TONE,
            ToneType.TONE,
            ToneType.SEMITONE
        ]
        super().__init__(notes, tone_type_sequence)
        self.name = "Major Mode"


class MixolidiumMode(ScaleMode):

    def __init__(self, notes):
        tone_type_sequence = [
            ToneType.TONE,
            ToneType.TONE,
            ToneType.SEMITONE,
            ToneType.TONE,
            ToneType.TONE,
            ToneType.SEMITONE,
            ToneType.TONE
        ]
        super().__init__(notes, tone_type_sequence)
        self.name = "Mixolidium Mode"


class LidiumMode(ScaleMode):

    def __init__(self, notes):
        tone_type_sequence = [
            ToneType.TONE,
            ToneType.TONE,
            ToneType.TONE,
            ToneType.SEMITONE,
            ToneType.TONE,
            ToneType.TONE,
            ToneType.SEMITONE
        ]
        super().__init__(notes, tone_type_sequence)
        self.name = "Lidium Mode"


class DoricMode(ScaleMode):

    def __init__(self, notes):
        tone_type_sequence = [
            ToneType.TONE,
            ToneType.SEMITONE,
            ToneType.TONE,
            ToneType.TONE,
            ToneType.TONE,
            ToneType.SEMITONE,
            ToneType.TONE
        ]
        super().__init__(notes, tone_type_sequence)
        self.name = "Doric Mode"


class FridgeoMode(ScaleMode):

    def __init__(self, notes):
        tone_type_sequence = [
            ToneType.SEMITONE,
            ToneType.TONE,
            ToneType.TONE,
            ToneType.TONE,
            ToneType.SEMITONE,
            ToneType.TONE,
            ToneType.TONE
        ]
        super().__init__(notes, tone_type_sequence)
        self.name = "Fridgeo Mode"


class LokriMode(ScaleMode):

    def __init__(self, notes):
        tone_type_sequence = [
            ToneType.SEMITONE,
            ToneType.TONE,
            ToneType.TONE,
            ToneType.TONE,
            ToneType.SEMITONE,
            ToneType.TONE,
            ToneType.TONE
        ]
        super().__init__(notes, tone_type_sequence)
        self.name = "Lokri Mode"


class MinorMode(ScaleMode):

    def __init__(self, notes):
        tone_type_sequence = [
            ToneType.TONE,
            ToneType.SEMITONE,
            ToneType.TONE,
            ToneType.TONE,
            ToneType.SEMITONE,
            ToneType.TONE,
            ToneType.TONE,
            ToneType.TONE
        ]
        super().__init__(notes, tone_type_sequence)
        self.name = "Minor Mode"
