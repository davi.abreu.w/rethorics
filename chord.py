class Chord():

    DEFAULT = "x"

    def __init__(self, notes, chord_n, base_note, size=5):
        self.notes = notes
        self.chord_n = chord_n
        self.base_note = base_note
        self.size = size

    def get_from_scale(self, scale):
        notes = [self.base_note]
        last_note = self.base_note
        for i in range(self.size + 1):
            last_note = self.notes.find_next_note(last_note)
            if last_note in scale:
                notes.append(last_note)
            else:
                notes.append(self.DEFAULT)
        return ("{} | " + "{!s:6}" * (self.size + 1)).format(self.chord_n, *notes)

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        notes = [self.base_note]
        last_note = self.base_note
        for i in range(self.size + 1):
            last_note = self.notes.find_next_note(last_note)
            notes.append(last_note)
        return ("{} | " +"{!s:4}" * (self.size + 1)).format(self.chord_n, *notes)
