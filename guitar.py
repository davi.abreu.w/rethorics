import chord

class Guitar():

    def __init__(self, notes, size=5):
        self.E_chord = chord.Chord(notes, 1, notes.E, size)
        self.a_chord = chord.Chord(notes, 2, notes.A, size)
        self.d_chord = chord.Chord(notes, 3, notes.D, size)
        self.g_chord = chord.Chord(notes, 4, notes.G, size)
        self.b_chord = chord.Chord(notes, 5, notes.B, size)
        self.e_chord = chord.Chord(notes, 6, notes.E, size)
        self.all_chords = [
            self.E_chord,
            self.a_chord,
            self.d_chord,
            self.g_chord,
            self.b_chord,
            self.e_chord
        ]

    def print_scale(self, scale):
        chords = [chord.get_from_scale(scale) for chord in self.all_chords]
        print(("{}\n" * len(chords)).format(*chords))
