from enum import Enum


class ToneType(Enum):
    TONE = "T"
    SEMITONE = "S"


class Note():

    def __init__(self, tp, symbol, show_type=True):
        self.type = tp
        self.symbol = symbol
        self.show_type = show_type

    def show_type(self):
        self.show_type = True

    def hide_type(self):
        self.show_type = False

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        if self.show_type:
            return "{} ({})".format(self.symbol, self.type.value)
        return "{}".format(self.symbol)


class Notes():

    def __init__(self):
        self.A = Note(ToneType.TONE, "A")
        self.As = Note(ToneType.SEMITONE, "A#")
        self.B = Note(ToneType.TONE, "B")
        self.C = Note(ToneType.TONE, "C")
        self.Cs = Note(ToneType.SEMITONE, "C#")
        self.D = Note(ToneType.TONE, "D")
        self.Ds = Note(ToneType.SEMITONE, "D#")
        self.E = Note(ToneType.TONE, "E")
        self.F = Note(ToneType.TONE, "F")
        self.Fs = Note(ToneType.SEMITONE, "F#")
        self.G = Note(ToneType.TONE, "G")
        self.Gs = Note(ToneType.SEMITONE, "G#")
        self.all_notes = [
            self.A,
            self.As,
            self.B,
            self.C,
            self.Cs,
            self.D,
            self.Ds,
            self.E,
            self.F,
            self.Fs,
            self.G,
            self.Gs
        ]

    def show_type(self):
        for note in self.all_notes:
            note.show_type()

    def hide_type(self):
        for note in self.all_notes:
            note.hide_type()

    def find_next_tone(self, actual_note):
        return self.find_next_note(actual_note, ToneType.TONE)

    def find_next_semitone(self, actual_note):
        return self.find_next_note(actual_note, ToneType.SEMITONE)

    def find_next_note(self, actual_note, tone_type=None):
        found_note = False
        for note in self.all_notes:
            if not found_note:
                found_note = note == actual_note
                continue
            if tone_type is None:
                return note
            elif tone_type == note.type:
                return note
        # Redo search from beggining
        for note in self.all_notes:
            if tone_type is None:
                return note
            elif tone_type == note.type:
                return note

    def find_note_index(self, note):
        return self.all_notes.index(note)

    def test(self):
        for note in self.all_notes:
            print("This is: {}".format(note))
            print("Next tone is: {}".format(self.find_next_tone(note)))
            print("Next semitone is: {}".format(self.find_next_semitone(note)))
            print()
